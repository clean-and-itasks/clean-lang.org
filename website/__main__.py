# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

import functools
import json
from klein import handle_errors, route, run
from twisted.internet import task
from twisted.web.static import File

import config
from db import Database
from exceptions import BadRequest, InternalServerError, NotFound, TwistedError
from jobs import UpdateVersionsJob, run_job
from package import Package

if config.run_jobs:
    print("Running jobs...")
    job_task = task.LoopingCall(run_job)
    job_task.start(config.job_timer)
else:
    print("Not running jobs...")

# Alternative route decorator for use in API endpoints
def apiroute(*args, **kwargs):
    def decorator(func):
        @route(*args, **kwargs)
        @functools.wraps(func)
        def inner(request, *args, **kwargs):
            request.setHeader('Content-Type', 'application/json')
            return func(request, *args, **kwargs)

        return inner

    return decorator

@handle_errors(BadRequest, NotFound, InternalServerError,
        FileNotFoundError)
def handle_bad_request(request, failure):
    if failure.type is BadRequest:
        request.setResponseCode(400)
    elif failure.type is NotFound or failure.type is FileNotFoundError:
        request.setResponseCode(404)
    elif failure.type is InternalServerError:
        request.setResponseCode(500)
    else:
        raise AttributeError('unknown exception')

    # On the api, return a plain-text error message. On other endpoints,
    # return a formatted page.
    if request.uri[:5] == b'/api/':
        return failure.getErrorMessage()
    else:
        return TwistedError(failure.getErrorMessage())

@apiroute('/api/packages/<name>', methods=['GET'])
def api_get_package(request, name):
    with Database() as db:
        package = db.get_package(name=name, with_versions=True)

        if package is None:
            raise NotFound('no such package')

        return json.dumps(package)

@apiroute('/api/packages/<name>', methods=['POST'])
def api_create_package(request, name):
    parameters = {}
    try:
        parameters = json.loads(request.content.read())
    except:
        raise BadRequest('failed to parse parameters')

    if 'url' not in parameters:
        raise BadRequest('url is required')
    elif not parameters['url'].startswith('https://gitlab.com/'):
        raise BadRequest('url must start with https://gitlab.com')

    with Database() as db:
        package = db.get_package(name=name)

        if package is None:
            try:
                id = db.create_package(name, parameters['url'])
                package = {'id': id}
            except:
                raise InternalServerError('failed to create package')
        else:
            if package['url'] != parameters['url']:
                raise BadRequest('url for existing package does not match')

        db.create_job(UpdateVersionsJob(package['id']))

        return json.dumps({'id': package['id']})

@apiroute('/api/packages', methods=['GET'])
def api_list_packages(request):
    with Database() as db:
        with_dependencies = \
                b'with_dependencies' in request.args and \
                request.args[b'with_dependencies'][-1] == b'true'
        with_full_metadata = \
                b'with_full_metadata' in request.args and \
                request.args[b'with_full_metadata'][-1] == b'true'
        packages = db.get_packages(
                with_dependencies=with_dependencies,
                with_full_metadata=with_full_metadata)
        # There may be no versions if the package was registered but versions
        # have not been fetched yet, or if an error occurred during fetching
        # them (e.g. private repo).
        packages = [p for p in packages if len(p['versions']) != 0]
        return bytes(json.dumps(packages), 'utf-8')

@route('/api', branch=True)
def api(request):
    raise NotFound('no such endpoint')

@route('/pkg/<pkg>/')
def package(request, pkg):
    try:
        return Package(pkg)
    except Exception as e:
        raise e
    except:
        raise InternalServerError('internal server error')

@route('/', branch=True)
def home(request):
    uri = request.uri
    if uri[-1] == ord('/'):
        uri = uri + b'index.html'
    if uri[-5:] == b'.html':
        head = open('public/layouts/head.html').read()
        content = open(b'public/endpoints/' + uri).read()
        foot = open('public/layouts/foot.html').read()
        return head + content + foot
    else:
        return File('public/endpoints/')

server = run('0.0.0.0', config.http_port, displayTracebacks=False)
