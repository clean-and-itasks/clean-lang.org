# Clean website

This is the source for the [Clean website](https://clean-lang.org).

## Installation

1. Create a MariaDB database and a user account with access to it.
2. Use `install/install.sql` to create the initial database structure.
3. Copy `.env.example` to `.env` and enter the MySQL details.
4. Run `run.sh`.
5. Optionally, configure a cronjob to run
   `check_stale_jobs.sh notify@maintainer.org` to regularly check and
   get notified about stalled jobs.

The website can be accessed at http://localhost:9190.

You can use the nginx config in `install/nginx.conf` to use nginx as a proxy
with a Let's Encrypt certificate.

## Author & License

This project is maintained by [TOP Software][].

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

Copyright is owned by the authors of individual commits, including:

- Gijs Alberts
- Mart Lubbers
- Steffen Michels
- Camil Staps

[Clean website]: https://clean-lang.org
[TOP Software]: https://top-software.nl