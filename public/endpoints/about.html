<div class="container px-3 py-3 pt-md-5 pb-md-4 mx-auto">
	<h1>About Clean</h1>
	<p>
		<a href="http://clean.cs.ru.nl" target="_blank"><strong>Clean</strong></a> is a <strong>general-purpose</strong>, <strong>pure</strong> and <strong>lazy</strong> functional programming language. <br>
		Clean is similar to <a href="https://haskell.org" target="_blank">Haskell</a> and distinguishes itself through <strong>increased performance</strong> and relatively <strong>faster compilation</strong> times. <br>
		It was created by researchers at the Radboud University Nijmegen (then Katholieke Universiteit Nijmegen). <br>
		It comes with a <strong>package manager</strong>, <a href="https://clean-and-itasks.gitlab.io/nitrile/" target="_blank">nitrile</a>.
	</p>
	<p>
		Clean intends to:
	</p>
	<ol>
		<li>Encourage programmers to think about <strong>versioning and backwards compatibility</strong>.</li>
		<li>Make it easy to <strong>install different versions</strong> of Clean and libraries alongside each other.</li>
		<li>Have <strong>stable builds</strong> using the GitLab Continuous Integration infrastructure.</li>
		<li><strong>Persist package versions</strong> decentrally in the GitLab package registries.</li>
	</ol>
	<p>
		On this page:
	</p>
	<ul>
		<li><a href="#names">About this website and Nitrile</a></li>
		<li><a href="#install">How do I install Clean?</a></li>
		<li><a href="#tooling">What tools can I use to develop with Clean?</a></li>
		<li><a href="#search">Package search tricks</a></li>
		<li><a href="#publishing">How can I publish my own package to the registry?</a></li>
		<li><a href="#conditions">What are the conditions for contributing to the registry?</a></li>
	</ul>

	<div class="card my-4 rounded-3 shadow-sm" id="names">
		<div class="card-header">
			<h4 class="my-0">About this Website and Nitrile</h4>
		</div>
		<div class="card-body">
			<p>
				This website aims to provide a basic Clean bundle as well as several libraries and a package manager, nitrile.
				These are collected in the central package registry (served by this website).
			</p>
			<p>
				The Clean <code>base</code> package contains (forked versions of) the Clean compiler, code generator, run-time system, build tools, and standard library.
				Upstream changes are regularly cherry-picked into these projects.
			</p>
			<p>
				<a href="https://clean-and-itasks.gitlab.io/nitrile/" target="_blank"><strong>Nitrile</strong></a> is a package manager and build tool for Clean.
				It is used to manage dependencies, interact with the package registry, and build applications.
			</p>
		</div>
	</div>

	<div class="card my-4 rounded-3 shadow-sm" id="install">
		<div class="card-header">
			<h4 class="my-0">How do I install Clean?</h4>
		</div>
		<div class="card-body">
			<ul class="nav nav-pills nav-fill" role="tablist">
				<li class="nav-item" role="presentation">
					<button class="nav-link active" id="install-tab-linux" data-bs-toggle="tab" data-bs-target="#install-linux" type="button" role="tab" aria-controls="install-linux" aria-selected="true">First install (Linux)</button>
				</li>
				<li class="nav-item" role="presentation">
					<button class="nav-link" id="install-tab-windows" data-bs-toggle="tab" data-bs-target="#install-windows" type="button" role="tab" aria-controls="install-windows" aria-selected="false">First install (Windows)</button>
				</li>
				<li class="nav-item" role="presentation">
					<button class="nav-link" id="install-tab-mac" data-bs-toggle="tab" data-bs-target="#install-mac" type="button" role="tab" aria-controls="install-windows" aria-selected="false">First install (Mac)</button>
				</li>
				<li class="nav-item" role="presentation">
					<button class="nav-link" id="install-tab-update" data-bs-toggle="tab" data-bs-target="#install-update" type="button" role="tab" aria-controls="install-update" aria-selected="false">Update an existing installation</button>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade pt-3 show active" id="install-linux" role="tabpanel" aria-labelledby="install-tab-linux">
					<p>
						The installation script depends on <code>curl</code>, <code>jq</code>, and <code>tar</code>.
						Nitrile itself depends on <code>curl</code>, <code>gcc</code>, and <code>tar</code>.
						After installing these packages, run:
					</p>
					<pre class="px-2 py-2 border bg-light"><code>curl https://clean-lang.org/install.sh | /bin/sh</code></pre>
					<p>
						(To install a specific Nitrile version (e.g. 0.4.0), add <code>-s 0.4.0</code> to the <code>/bin/sh</code> arguments.)
					</p>
					<p>
						Afterwards, be sure to add <code>~/.nitrile/bin</code> to your path.
					</p>
					<p>
						You may now want to follow the <a href="https://clean-and-itasks.gitlab.io/nitrile/intro/getting-started/" target="_blank">Nitrile getting started</a> guide.
					</p>
				</div>
				<div class="tab-pane fade pt-3" id="install-windows" role="tabpanel" aria-labelledby="install-tab-windows">
					<p>
						Nitrile depends on <a href="https://chocolatey.org/" target="_blank">Chocolatey</a>.
						Make sure you <a href="https://chocolatey.org/install" target="_blank">install Chocolatey</a> before installing Nitrile.
					</p>
					<p>
						Now run the following in an <strong>administrative shell</strong>:
					</p>
					<pre class="px-2 py-2 border bg-light"><code>Set-ExecutionPolicy ByPass -Scope Process -Force; (New-Object System.Net.WebClient).DownloadString('https://clean-lang.org/install.ps1') | powershell -Command -</code></pre>
					<p>
						(To install a specific Nitrile version (e.g. 0.4.0), run <code>$Env:NitrileVersion = '0.4.0'</code> beforehand.)
					</p>
					<p>
						You may need to reboot for <code>nitrile</code> to be available in your path.
					</p>
					<p>
						You may now want to follow the <a href="https://clean-and-itasks.gitlab.io/nitrile/intro/getting-started/" target="_blank">Nitrile getting started</a> guide.
					</p>
				</div>
				<div class="tab-pane fade pt-3" id="install-mac" role="tabpanel" aria-labelledby="install-tab-mac">
					<p>
						Clean via Nitrile does not run natively on Mac (neither x64 nor ARM) but it is possible to use it via <a href="https://www.docker.com/" target="_blank">Docker</a> (for example in a <a href="https://containers.dev/" target="_blank">devcontainer</a>).
						The Docker image <code>cleanlang/devcontainer:latest</code> contains all the tools to use Nitrile (including Nitrile itself).
						For an in depth tutorial on how to set up a devcontainer, see the <a href="https://gitlab.com/clean-and-itasks/itasks-template" target="_blank">iTasks template repository</a>.
					</p>
					<p>
						You may now want to follow the <a href="https://clean-and-itasks.gitlab.io/nitrile/intro/getting-started/" target="_blank">Nitrile getting started</a> guide.
					</p>
				</div>
				<div class="tab-pane fade pt-3" id="install-update" role="tabpanel" aria-labelledby="install-tab-update">
					<p>
						Simply run:
					</p>
					<pre class="px-2 py-2 border bg-light"><code>nitrile update
nitrile global install nitrile
nitrile global remove nitrile OLD_NITRILE_VERSION</code></pre>
					<p>
						You can list all installed Nitrile versions with <code>nitrile global list</code>.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="card my-4 rounded-3 shadow-sm" id="tooling">
		<div class="card-header">
			<h4 class="my-0">What tools can I use to develop with Clean?</h4>
		</div>
		<div class="card-body">
			<p>There are several dedicated editor plugins used to develop in Clean, the plugins below provide syntax highlighting and may provide several other features:</p>
			<p><strong>Actively Maintained:</strong></p>
			<ul>
				<li><a target="_blank" href="https://gitlab.com/clean-and-itasks/vim-clean">Vim</a></li>
				<li><a target="_blank" href="https://gitlab.com/top-software/clean-vs-code">Visual Studio Code</a></li>
			</ul>
			<p>Unmaintained:</p>
			<ul>
				<li><a target="_blank" href="https://github.com/timjs/atom-language-clean">Atom</a></li>
				<li><a target="_blank" href="https://github.com/matheusamazonas/sublime3_clean">Sublime Text 3</a></li>
			</ul>
			<p>There is a dedicated <strong>search engine</strong> for the entire nitrile registry:</p>
			<ul>
				<li>
					<a target="_blank" href="http://cloogle.org/">Cloogle</a>: a search engine for Clean.
					It indexes the packages in the registry, and is updated nightly.
				</li>
			</ul>
			<p>The following <strong>highlighters</strong> can be used:</p>
			<ul>
				<li><a target="_blank" href="https://www.npmjs.com/package/highlight.js">JavaScript (highlight.js)</a></li>
				<li><a target="_blank" href="https://www.npmjs.com/package/clean-highlighter">JavaScript (standalone)</a></li>
				<li><a target="_blank" href="https://pypi.python.org/pypi/pygments-lexer-clean">Python (pygments)</a></li>
			</ul>
		</div>
	</div>

	<div class="card my-4 rounded-3 shadow-sm" id="search">
		<div class="card-header">
			<h4 class="my-0">Package search tricks</h4>
		</div>
		<div class="card-body">
			<p>
				You can search in a package's title, description, readme, and changelog.
				The following tricks can be used:
			</p>
			<ul>
				<li>Wildcards: <code>foo*</code> to match words beginning with <code>foo</code> (also <code>*oo</code>; <code>f*o</code>)</li>
				<li>Fields: <code>title:foo</code>, <code>description:foo</code>, <code>readme:foo</code>, <code>changelog:foo</code> to limit a term to one field</li>
				<li>Boosts: <code>foo^5 bar</code> to make <code>foo</code> 5 times more important than <code>bar</code></li>
				<li>Forced inclusion and exclusion: <code>+foo bar -baz</code> must include <code>foo</code> and may not include <code>baz</code></li>
			</ul>
			<p>
				See the <a href="https://lunrjs.com/guides/searching.html" target="_blank">lunr.js search guide</a> for more details.
			</p>
		</div>
	</div>

	<div class="card my-4 rounded-3 shadow-sm" id="publishing">
		<div class="card-header">
			<h4 class="my-0">How can I publish my own package to the registry?</h4>
		</div>
		<div class="card-body">
			<p>
				See the <a href="https://clean-and-itasks.gitlab.io/nitrile/packaging-and-publishing/intro/" target="_blank">Nitrile instructions</a>.
			</p>
		</div>
	</div>

	<div class="card my-4 rounded-3 shadow-sm" id="conditions">
		<div class="card-header">
			<h4 class="my-0">What are the conditions for contributing to the registry?</h4>
		</div>
		<div class="card-body">
			<p>
				Contributions should be in line with the project goals describe <a href="#">above</a>.
				The project maintainer retains the right to remove any package at any time without giving a reason.
			</p>
		</div>
	</div>
</div>
