/**
 * Copyright 2021-2024 the authors (see README.md).
 *
 * This file is part of the Clean website.
 *
 * The Clean website is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * The Clean website is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the Clean website. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

function parseVersion(v) {
	return v.split('.').map(Number);
}

/**
 * Returns true if v is the highest minor version within a particular major
 * version, that is, if there is no version with the same major version but a
 * higher minor version. Patch numbers are ignored.
 */
function highestMinor(v, versions) {
	v = parseVersion(v);
	return versions.map(parseVersion).filter(v2 => v[0] == v2[0] && v[1] < v2[1]).length == 0;
}

function earlierVersion(x, y) {
	return y.versions[y.latest_version].created - x.versions[x.latest_version].created;
}

function searchPackages(query, index, pkgs) {
	let results = [];

	if (query == '')
		return results;

	for (let result of index.search(query))
		results.push(pkgs[result.ref]);

	return results;
}

function getLatestUpdates(pkgs) {
	pkgs = Object.values(pkgs).filter(p => Object.keys(p.versions).length > 0);
	pkgs.sort(earlierVersion);
	return pkgs.slice(0, 10);
}

function makePackageCard(pkg) {
	let card = document.createElement('div');
	card.classList.add('card', 'mb-4', 'rounded-3', 'shadow-sm');

	let header = document.createElement('a');
	header.classList.add('card-header', 'link-dark', 'text-decoration-none');
	header.href = '/pkg/' + pkg.name;
	header.innerHTML = '<h6 class="my-0 fw-bold">' + pkg.name +
		(pkg.deprecated ? '&emsp;<span class="badge bg-light text-dark border border-dark">deprecated</span>' : '') +
		'</h6>';
	card.appendChild(header);

	let body = document.createElement('div');
	body.classList.add('card-body');
	card.appendChild(body);

	let metadata = document.createElement('p');
	metadata.classList.add('mb-1');
	let homepage = document.createElement('a');
	homepage.classList.add('link-secondary');
	homepage.href = pkg.url;
	homepage.innerText = 'Homepage';
	homepage.target = '_blank';
	metadata.appendChild(homepage);
	metadata.innerHTML += '&ensp;&bullet;&ensp;';
	let more_info = document.createElement('a');
	more_info.classList.add('link-secondary');
	more_info.href = '/pkg/' + pkg.name;
	more_info.innerText = 'More info';
	metadata.appendChild(more_info);
	metadata.innerHTML += '&ensp;&bullet;&ensp;';
	let license = document.createElement('a');
	license.classList.add('text-muted', 'text-decoration-none');
	license.href = 'https://spdx.org/licenses/' + pkg.license + '.html';
	license.target = '_blank';
	license.innerText = pkg.license;
	metadata.appendChild(license);
	if (Object.keys(pkg.versions).length >= 1) {
		metadata.innerHTML += '&ensp;&bullet;&ensp;';
		let last_version = Math.max(...Object.values(pkg.versions).map(v => v.created));
		const dateOptions = {weekday: 'short', year: 'numeric', month: 'short', day: 'numeric'};
		metadata.innerHTML += 'Last updated ' + new Date(last_version*1000).toLocaleDateString('en-GB', dateOptions);
	}
	body.appendChild(metadata);

	let description = document.createElement('p');
	description.classList.add('mb-1');
	description.innerText = pkg.description;
	body.appendChild(description);

	let versions = document.createElement('p');
	versions.classList.add('mb-0', 'text-muted');
	let versions_list = Object.keys(pkg.versions);
	if (versions_list.length == 0) {
		versions.innerHTML = '<small>No versions known.</small>';
	} else {
		let version_groups = [];
		for (let version of versions_list) {
			if (version_groups.length != 0 && version_groups[version_groups.length-1][0].replace (/\.\d+$/,'') == version.replace (/\.\d+$/,''))
				version_groups[version_groups.length-1][1] = version.replace (/^.*\.(\d+)$/,'$1');
			else
				version_groups.push ([version]);
		}
		version_groups = version_groups.map(range => {
			let highest = highestMinor (range[0],versions_list);
			if (range.length == 2){
				if (highest) {
					return range[0] + '..<b>' + range[1] + '</b>';
				} else {
					return range[0] + '..' + range[1];
				}
			} else if (highest) {
				return '<b>' + range[0] + '</b>';
			} else {
				return range[0];
			}
		});
		versions.innerHTML = '<small>Version(s): ' +  version_groups.join(', ') + '.</small>';
	}
	body.appendChild(versions);

	return card;
}

function setupSearch() {
	let search_field = document.getElementById('package-search');
	if (search_field) {
		let packages = [];
		let index = null;

		search_field.disabled = true;
		search_field.onkeyup = function () {
			search_field.classList.remove('is-invalid');
			search_field.setCustomValidity('');
			try {
				let pkgs = this.value.length == 0 ? getLatestUpdates(packages) : searchPackages(this.value, index, packages);

				search_results.innerHTML = '';
				let title = document.createElement('h1');
				title.classList.add('mb-3');
				search_results.appendChild(title);

				if (this.value.length == 0) {
					title.innerHTML = 'Latest package updates';
				} else if (pkgs.length == 0) {
					title.innerHTML = 'No results...';
					return;
				} else {
					title.innerHTML = 'Search results';
				}

				for (let pkg of pkgs)
					search_results.appendChild(makePackageCard(pkg));
			} catch (e) {
				search_field.classList.add('is-invalid');

				if (e.name == 'QueryParseError') {
					search_field.setCustomValidity('Invalid query: ' + e.message);
				} else {
					search_field.setCustomValidity('Search failed: ' + e.message);
				}
			}
		};

		let search_results = document.getElementById('package-results');

		let xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
			if (this.readyState != 4)
				return;

			if (this.status != 200) {
				window.alert('Failed to load package information. The website will not work as expected.');
				return;
			}

			let responseText = this.responseText;
			packages = {};
			index = lunr(function(){
				this.field('name', {boost: 30});
				this.field('description', {boost: 10});
				this.field('readme', {boost: 3});
				this.field('changelog');

				for (let pkg of JSON.parse(responseText)) {
					packages[pkg.id] = pkg;
					this.add(pkg);
				}
			});

			search_field.disabled = false;
			search_field.onkeyup();
			search_field.focus();
		};
		xhttp.open('GET', '/api/packages?with_full_metadata=true', true);
		xhttp.send();
	}
}

function markdownify() {
	let markdown = window.markdownit({
		linkify: true,
		typographer: true,
		highlight: function (str, lang) {
			if (lang && hljs.getLanguage(lang)) {
				try {
					return '<pre class="px-2 py-2 border bg-light"><code>' +
							hljs.highlight(str, {language: lang, ignoreIllegals: true}).value +
						'</code></pre>';
				} catch (__) {}
			}
			return '<pre class="px-2 py-2 border bg-light"><code>' + markdown.utils.escapeHtml(str) + '</code></pre>';
		}
	});

	markdown.linkify.tlds('.md', false);

	let org_normalizeLink = markdown.normalizeLink;
	document.querySelectorAll('.markdown').forEach(div => {
		/* Don't allow local links, they won't work */
		markdown.normalizeLink = function (url) {
			/* Local link */
			if (!/^(https?|ftp|mailto):/.test(url)) {
				if ('gitlabUrl' in div.dataset && 'latestVersion' in div.dataset) {
					if (url[0] == '/')
						return div.dataset.gitlabUrl + '/-/blob/v' + div.dataset.latestVersion + url;
					return div.dataset.gitlabUrl + '/-/blob/v' + div.dataset.latestVersion + '/' + url;
				} else {
					return '#';
				}
			}
			return org_normalizeLink(url);
		};

		div.innerHTML = markdown.render(div.textContent.trim());
		div.querySelectorAll('a').forEach(a => a.target = '_blank');
	});
}

(() => {
	setupSearch();
	markdownify();
})();
