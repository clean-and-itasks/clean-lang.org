# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

$Identity = [Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()
If (-NOT $Identity.IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
	Write-Error "This script must be run as an administrator!"
	exit 1
}

Write-Output "Checking if chocolatey is installed..."
try {
	choco --version | Out-Null
} catch [System.Management.Automation.CommandNotFoundException] {
	Write-Error "Install chocolatey first. Head over to https://chocolatey.org/install."
	exit 1
}

Write-Output "Installing nitrile dependencies with chocolatey..."
choco install -y curl jq pwsh vcredist140

$NitrilePackageContent = curl.exe -s https://clean-lang.org/api/packages/nitrile url.exe

Write-Output "Fetching version information..."
if (Test-Path Env:NitrileVersion) {
	$JqNitrileVersion = "\`"$Env:NitrileVersion\`""
	$NitrileVersion = $Env:NitrileVersion
} else {
	$JqNitrileVersion = ".latest_version"
	$NitrileVersion = $NitrilePackageContent | jq $JqNitrileVersion
}
$NitrileVersion = $NitrileVersion.Trim('"')
$NitrileDownloadUrl = $NitrilePackageContent | jq ".versions[$JqNitrileVersion].targets.\`"windows-x64\`".url"
Write-Output $NitrileDownloadUrl

if ($NitrileDownloadUrl -eq "null") {
	if (Test-Path Env:NitrileVersion) {
		Write-Error "Could not find version $Env:NitrileVersion (a full version must be specified, e.g. 0.4.0 instead of 0.4)."
	} else {
		Write-Error "Failed to find the latest version."
	}
	exit 1
}

cd $Env:APPDATA

If (Test-Path "Nitrile") {
	Write-Error "There is already a directory $Env:APPDATA\Nitrile. Remove this directory to start with a clean slate."
	exit 1
}

Write-Output "Installing to $Env:APPDATA\Nitrile."
cd $Env:APPDATA
mkdir Nitrile | Out-Null
cd Nitrile
mkdir bin | Out-Null
mkdir packages | Out-Null
mkdir packages\nitrile | Out-Null
mkdir packages\nitrile\$NitrileVersion-windows-x64 | Out-Null

Write-Output "Downloading nitrile..."
curl.exe -L -`# $NitrileDownloadUrl -o nitrile.tar.gz
tar xzpf nitrile.tar.gz -C packages\nitrile\$NitrileVersion-windows-x64 --strip-components=1
rm nitrile.tar.gz

Write-Output "Installing links to executables..."
Get-ChildItem -Path packages\nitrile\$NitrileVersion-windows-x64\bin | Foreach-Object {
	$Name = $_.Name
	New-Item -ItemType SymbolicLink -Path "bin\$Name" -Target $_.FullName | Out-Null
}

Write-Output "Updating PATH..."
$Path = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
If ($Path.split(';') -contains "$Env:APPDATA\Nitrile\bin") {
	Write-Output "PATH already contains $Env:APPDATA\Nitrile\bin"
	Write-Output "All done."
} Else {
	$Path += ";$Env:APPDATA\Nitrile\bin"
	Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $Path
	Write-Output "All done. You may need to reboot for nitrile to appear in your PATH."
}
