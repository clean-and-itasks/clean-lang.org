#!/bin/sh

# Copyright 2021-2024 the authors (see README.md).
#
# This file is part of the Clean website.
#
# The Clean website is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3 of the License.
#
# The Clean website is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with the Clean website. If not, see
# <https://www.gnu.org/licenses/>.
#
# The software is licensed under additional terms under section 7 of the GNU
# Affero General Public License; see the LICENSE file for details.

HOST_PORT=9190

set -ev

npm ci

cp node_modules/bootstrap/dist/js/bootstrap.min.js public/endpoints/js
cp node_modules/bootstrap/dist/css/bootstrap.min.css public/endpoints/css
cp node_modules/bootstrap/dist/css/bootstrap.min.css.map public/endpoints/css

cp node_modules/markdown-it/dist/markdown-it.min.js public/endpoints/js

cp node_modules/lunr/lunr.min.js public/endpoints/js

docker build -t clean/website .
docker rm -f clean-website 2>/dev/null
docker run -d \
	--name clean-website \
	--restart=unless-stopped \
	-v "$PWD"/public:/usr/src/app/public \
	-p $HOST_PORT:80 \
	clean/website
